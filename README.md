# README #

This is implementation of algorithms:

* binary search
* counting sort
* insert sort
* quick sort
* Shell sort

Each has tests (JUnit 4)

# CONTACTS #

VK: https://vk.com/pujari

EMAIL: shiawasena.hoshi@gmail.com